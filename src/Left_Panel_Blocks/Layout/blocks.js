export default function (editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  let blocks = c.blocks;
  let stylePrefix = c.stylePrefix;
  const flexGrid = c.flexGrid;
  const basicStyle = c.addBasicStyle;
  const clsRow = `${stylePrefix}row`;
  const clsCell = `${stylePrefix}cell`;
  const styleRow = flexGrid ? `
    .${clsRow} {
      display: flex;
      justify-content: flex-start;
      align-items: stretch;
      flex-wrap: nowrap;
      padding: 10px;
    }
    @media (max-width: 768px) {
      .${clsRow} {
        flex-wrap: wrap;
      }
    }` : `
    .${clsRow} {
      display: table;
      padding: 10px;
      width: 100%;
    }
    @media (max-width: 768px) {
      .${stylePrefix}cell, .${stylePrefix}cell30, .${stylePrefix}cell70 {
        width: 100%;
        display: block;
      }
    }`;
  const styleClm = flexGrid ? `
    .${clsCell} {
      min-height: 75px;
      flex-grow: 1;
      // flex-basis: 100%;
    }` : `
    .${clsCell} {
      // width: 8%;
      display: table-cell;
      height: 75px;
    }`;
 
  const step = 0.2;
  const minDim = 1;
  const currentUnit = 1;
  const resizerBtm = { tl: 1, tc: 1, tr: 1, cl:1, cr:1, bl:1, br: 1, minDim };
  const resizerRight = { ...resizerBtm, cr: 1, bc: 1, currentUnit, minDim, step };

  // Flex elements do not react on width style change therefore I use
  // 'flex-basis' as keyWidth for the resizer on columns
  if (flexGrid) {
    resizerRight.keyWidth = 'flex-basis';
  }

  const rowAttr = {
    class: clsRow,
    'data-gjs-droppable': `.${clsCell}`,
    'data-gjs-resizable': resizerBtm,
    'data-gjs-name': 'Row',
  };

  const colAttr = {
    class: clsCell,
    'data-gjs-draggable': `.${clsRow}`,
    'data-gjs-resizable': resizerRight,
    'data-gjs-name': 'Cell',
  };

  if (flexGrid) {
    colAttr['data-gjs-unstylable'] = ['width'];
    colAttr['data-gjs-stylable-require'] = ['flex-basis'];
  }

  // Make row and column classes private
  const privateCls = [`.${clsRow}`, `.${clsCell}`];
  editor.on('selector:add', selector =>
    privateCls.indexOf(selector.getFullName()) >= 0 && selector.set('private', 1))

  const attrsToString = attrs => {
    const result = [];

    for (let key in attrs) {
      let value = attrs[key];
      const toParse = value instanceof Array || value instanceof Object;
      value = toParse ? JSON.stringify(value) : value;
      result.push(`${key}=${toParse ? `'${value}'` : `"${value}"`}`);
    }

    return result.length ? ` ${result.join(' ')}` : '';
  }

  const toAdd = name => blocks.indexOf(name) >= 0;
  const attrsRow = attrsToString(rowAttr);
  const attrsCell = attrsToString(colAttr);

  toAdd('emptyrow') && bm.add('emptyrow', {
    label:'<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="51px" height="27.003px" viewBox="0 0 51 27.003" enable-background="new 0 0 51 27.003" xml:space="preserve"> <path fill="#FFFFFF" stroke="#000000" stroke-miterlimit="10" d="M50.5,24.152c0,1.297-1.051,2.348-2.348,2.348H2.848 c-1.296,0-2.348-1.051-2.348-2.348V2.848C0.5,1.551,1.551,0.5,2.848,0.5h45.305c1.296,0,2.348,1.051,2.348,2.348V24.152z"/> </svg><div class="gjs-block-label">Empty Row</div>',
    category: c.category,
    // attributes: {class:'gjs-fonts gjs-f-b1'},
    content: `<div ${attrsRow}>
        <div ${attrsCell}></div>
      </div>
      ${ basicStyle ?
        `<style>
          ${styleRow}
          ${styleClm}
        </style>`
        : ''}`
  });
  toAdd('emptycolumn') && bm.add('emptycolumn', {
    label:'<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="50px" height="25.005px" viewBox="0 0 50 25.005" enable-background="new 0 0 50 25.005" xml:space="preserve"> <path fill="#FFFFFF" stroke="#000000" stroke-miterlimit="10" d="M15.5,21.521c0,1.657-1.343,3-3,3h-9c-1.657,0-3-1.343-3-3v-18 c0-1.657,1.343-3,3-3h9c1.657,0,3,1.343,3,3V21.521z"/> <path fill="#FFFFFF" stroke="#000000" stroke-miterlimit="10" d="M32.5,21.521c0,1.657-1.343,3-3,3h-9c-1.657,0-3-1.343-3-3v-18 c0-1.657,1.343-3,3-3h9c1.657,0,3,1.343,3,3V21.521z"/> <path fill="#FFFFFF" stroke="#000000" stroke-miterlimit="10" d="M49.5,21.521c0,1.657-1.343,3-3,3h-9c-1.657,0-3-1.343-3-3v-18 c0-1.657,1.343-3,3-3h9c1.657,0,3,1.343,3,3V21.521z"/> </svg><div class="gjs-block-label">Empty Column</div>',
    category: c.category,
    draggable: true,
    // attributes: {class:'gjs-fonts gjs-f-b1'},
    content:`<div ${attrsRow}>
    <div ${attrsCell}></div>
    <div ${attrsCell}></div>
    <div ${attrsCell}></div>
  </div>
  ${ basicStyle ?
    `<style>
      ${styleRow}
      ${styleClm}
    </style>`
    : ''}`
  });


  toAdd('object2') && bm.add('object2', {
    label:'<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="51px" height="27.003px" viewBox="0 0 51 27.003" enable-background="new 0 0 51 27.003" xml:space="preserve"> <path fill="#FFFFFF" stroke="#000000" stroke-miterlimit="10" d="M50.5,24.152c0,1.297-1.051,2.348-2.348,2.348H2.848 c-1.296,0-2.348-1.051-2.348-2.348V2.848C0.5,1.551,1.551,0.5,2.848,0.5h45.305c1.296,0,2.348,1.051,2.348,2.348V24.152z"/> <path d="M25,22.043C25,23.676,23.676,25,22.043,25H4.957C3.324,25,2,23.676,2,22.043V4.957C2,3.324,3.324,2,4.957,2h17.086 C23.676,2,25,3.324,25,4.957V22.043z"/> <path d="M49,22.043C49,23.676,47.676,25,46.042,25H28.958C27.324,25,26,23.676,26,22.043V4.957C26,3.324,27.324,2,28.958,2h17.085 C47.676,2,49,3.324,49,4.957V22.043z"/> </svg><div class="gjs-block-label">2 Object</div>',
    // attributes: {class:'gjs-fonts gjs-f-b2'},
    category: c.category,
    type:'2object',
    content:{type:'2object'}
    // content: `<div ${attrsRow}>
    //     <div ${attrsCell}></div>
    //     <div ${attrsCell}></div>
    //   </div>
    //   ${ basicStyle ?
    //     `<style>
    //       ${styleRow}
    //       ${styleClm}
    //     </style>`
    //     : ''}`
  });

  toAdd('object3') && bm.add('object3', {
    label:'<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="51px" height="27.003px" viewBox="0 0 51 27.003" enable-background="new 0 0 51 27.003" xml:space="preserve"> <path fill="#FFFFFF" stroke="#000000" stroke-miterlimit="10" d="M50.5,24.152c0,1.297-1.051,2.348-2.348,2.348H2.848 c-1.296,0-2.348-1.051-2.348-2.348V2.848C0.5,1.551,1.551,0.5,2.848,0.5h45.305c1.296,0,2.348,1.051,2.348,2.348V24.152z"/> <path d="M25,22.043C25,23.676,23.676,25,22.043,25H4.957C3.324,25,2,23.676,2,22.043V4.957C2,3.324,3.324,2,4.957,2h17.086 C23.676,2,25,3.324,25,4.957V22.043z"/> <path d="M49,12c0,0.553-0.448,1-1,1H27c-0.552,0-1-0.447-1-1V3c0-0.553,0.448-1,1-1h21c0.552,0,1,0.447,1,1V12z"/> <path d="M49,24c0,0.553-0.448,1-1,1H27c-0.552,0-1-0.447-1-1v-9c0-0.553,0.448-1,1-1h21c0.552,0,1,0.447,1,1V24z"/> </svg><div class="gjs-block-label">3 Object</div>',
    category: c.category,
    selectable:false,
    type:'3object',
    content:{type:'3object'}
    // attributes: {class:'gjs-fonts gjs-f-b3'},
//     content:  `<div ${attrsRow}>
//     <div ${attrsCell}></div>
//     <div ${attrsCell}><div ${attrsRow}>
//     <div ${attrsCell}></div>
//   </div><div ${attrsRow}>
//   <div ${attrsCell}></div>
// </div></div>
//   </div>
//   ${ basicStyle ?
//     `<style>
//       ${styleRow}
//       ${styleClm}
//     </style>`
//     : ''}`
  });

  toAdd('emptytab') && bm.add('emptytab', {
    label: '<svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="55.000000pt" height="25.000000pt" viewBox="0 0 124.000000 52.000000" preserveAspectRatio="xMidYMid meet"><g transform="translate(0.000000,52.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path d="M4 485 c-3 -14 -3 -38 0 -53 4 -15 6 -31 5 -37 -1 -5 -3 -81 -6 -169 -4 -152 -3 -160 19 -192 l23 -34 575 0 575 0 23 33 c20 31 22 44 22 182 0 148 0 149 -27 177 l-27 28 -363 0 -363 0 -6 25 c-3 14 -18 34 -31 45 -24 18 -41 20 -219 20 l-194 0 -6 -25z m1191 -109 c14 -21 16 -50 13 -175 -2 -106 -7 -154 -16 -163 -10 -10 -127 -13 -572 -13 -547 0 -559 0 -574 20 -12 16 -15 52 -16 172 0 146 1 152 22 167 20 14 91 16 575 16 l553 0 15 -24z"/></g></svg><div class="gjs-block-label">Empty Tab</div>',
    category: c.category,
    category: c.category,
    type:'empttabs',
    content:{type:'empttabs'}
   // type:'tabs',
    //content:'<div data-gjs-type="tabs"></div><style>.tab{text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;color:inherit;padding-top:7px;padding-right:14px;padding-bottom:7px;padding-left:14px;transition-duration:.3s;transition-timing-function:ease;transition-delay:0s;transition-property:opacity;display:inline-block;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;margin-right:10px}.tab.tab-active{background-color:rgb(13, 148, 230);color:white;}.tab-content{padding-top:6px;padding-right:12px;padding-bottom:6px;padding-left:12px;min-height:100px;animation-duration:1s;animation-timing-function:ease;animation-delay:0s;animation-iteration-count:1;animation-direction:normal;animation-fill-mode:none;animation-play-state:running;animation-name:fadeEffect}@keyframes fadeEffect{0%{opacity:0;}100%{opacity:1;}}</style>'
    // attributes: {class:'gjs-fonts gjs-f-b37'},
    // content: `<div ${attrsRow}>
    //     <div ${attrsCell} style="${flexGrid ? 'flex-basis' : 'width'}: 30%;"></div>
    //     <div ${attrsCell} style="${flexGrid ? 'flex-basis' : 'width'}: 70%;"></div>
    //   </div>
    //   ${ basicStyle ?
    //     `<style>
    //       ${styleRow}
    //       ${styleClm}
    //       ${styleClm30}
    //       ${styleClm70}
    //     </style>`
    //     : ''}`
  });

  toAdd('object4') && bm.add('object4', {
    label:'<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="51px" height="27.003px" viewBox="0 0 51 27.003" enable-background="new 0 0 51 27.003" xml:space="preserve"> <path fill="#FFFFFF" stroke="#000000" stroke-miterlimit="10" d="M50.5,24.152c0,1.297-1.051,2.348-2.348,2.348H2.848 c-1.296,0-2.348-1.051-2.348-2.348V2.848C0.5,1.551,1.551,0.5,2.848,0.5h45.305c1.296,0,2.348,1.051,2.348,2.348V24.152z"/> <path d="M49,12c0,0.553-0.448,1-1,1H27c-0.552,0-1-0.447-1-1V3c0-0.553,0.448-1,1-1h21c0.552,0,1,0.447,1,1V12z"/> <path d="M49,24c0,0.553-0.448,1-1,1H27c-0.552,0-1-0.447-1-1v-9c0-0.553,0.448-1,1-1h21c0.552,0,1,0.447,1,1V24z"/> <path d="M25,12c0,0.553-0.448,1-1,1H3c-0.552,0-1-0.447-1-1V3c0-0.553,0.448-1,1-1h21c0.552,0,1,0.447,1,1V12z"/> <path d="M25,24c0,0.553-0.448,1-1,1H3c-0.552,0-1-0.447-1-1v-9c0-0.553,0.448-1,1-1h21c0.552,0,1,0.447,1,1V24z"/> </svg><div class="gjs-block-label">4 Object</div>',
    category: c.category,
    type:'4object',
    content:{type:'4object'}
    // attributes: {class:'gjs-fonts gjs-f-text'},
//     content:  `<div ${attrsRow}>
//     <div ${attrsCell}><div ${attrsRow}>
//     <div ${attrsCell}></div>
//   </div><div ${attrsRow}>
//   <div ${attrsCell}></div>
// </div></div>
//     <div ${attrsCell}><div ${attrsRow}>
//     <div ${attrsCell}></div>
//   </div><div ${attrsRow}>
//   <div ${attrsCell}></div>
// </div></div>
//   </div>
//   ${ basicStyle ?
//     `<style>
//       ${styleRow}
//       ${styleClm}
//     </style>`
//     : ''}`
  });

  toAdd('object5') && bm.add('object5', {
    label:'<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="51px" height="27.003px" viewBox="0 0 51 27.003" enable-background="new 0 0 51 27.003" xml:space="preserve"> <path fill="#FFFFFF" stroke="#000000" stroke-miterlimit="10" d="M50.5,24.152c0,1.297-1.051,2.348-2.348,2.348H2.848 c-1.296,0-2.348-1.051-2.348-2.348V2.848C0.5,1.551,1.551,0.5,2.848,0.5h45.305c1.296,0,2.348,1.051,2.348,2.348V24.152z"/> <path d="M25,22.043C25,23.676,23.676,25,22.043,25H4.957C3.324,25,2,23.676,2,22.043V4.957C2,3.324,3.324,2,4.957,2h17.086 C23.676,2,25,3.324,25,4.957V22.043z"/> <path d="M37,12c0,0.553-0.448,1-1,1h-9c-0.552,0-1-0.447-1-1V3c0-0.553,0.448-1,1-1h9c0.552,0,1,0.447,1,1V12z"/> <path d="M49,12c0,0.553-0.448,1-1,1h-9c-0.552,0-1-0.447-1-1V3c0-0.553,0.448-1,1-1h9c0.552,0,1,0.447,1,1V12z"/> <path d="M37,24c0,0.553-0.448,1-1,1h-9c-0.552,0-1-0.447-1-1v-9c0-0.553,0.448-1,1-1h9c0.552,0,1,0.447,1,1V24z"/> <path d="M49,24c0,0.553-0.448,1-1,1h-9c-0.552,0-1-0.447-1-1v-9c0-0.553,0.448-1,1-1h9c0.552,0,1,0.447,1,1V24z"/> </svg><div class="gjs-block-label">5 Object</div>',
    category: c.category,
    type:'5object',
    content:{type:'5object'}
    // attributes: {class:'fa fa-link'},
//     content: `<div ${attrsRow}> <div ${attrsCell}></div>
//     <div ${attrsCell}><div ${attrsRow}>
//     <div ${attrsCell}></div>
//   </div><div ${attrsRow}>
//   <div ${attrsCell}></div>
// </div></div>
//     <div ${attrsCell}><div ${attrsRow}>
//     <div ${attrsCell}></div>
//   </div><div ${attrsRow}>
//   <div ${attrsCell}></div>
// </div></div>
//   </div>
//   ${ basicStyle ?
//     `<style>
//       ${styleRow}
//       ${styleClm}
//     </style>`
//     : ''}`
  });

  toAdd('object6') && bm.add('object6', {
   label:'<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="51px" height="27.003px" viewBox="0 0 51 27.003" enable-background="new 0 0 51 27.003" xml:space="preserve"> <path fill="#FFFFFF" stroke="#000000" stroke-miterlimit="10" d="M50.5,24.152c0,1.297-1.051,2.348-2.348,2.348H2.848 c-1.296,0-2.348-1.051-2.348-2.348V2.848C0.5,1.551,1.551,0.5,2.848,0.5h45.305c1.296,0,2.348,1.051,2.348,2.348V24.152z"/> <path d="M17,12c0,0.553-0.448,1-1,1H3c-0.552,0-1-0.447-1-1V3c0-0.553,0.448-1,1-1h13c0.552,0,1,0.447,1,1V12z"/> <path d="M33,12c0,0.553-0.448,1-1,1H19c-0.552,0-1-0.447-1-1V3c0-0.553,0.448-1,1-1h13c0.552,0,1,0.447,1,1V12z"/> <path d="M49,12c0,0.553-0.448,1-1,1H35c-0.552,0-1-0.447-1-1V3c0-0.553,0.448-1,1-1h13c0.552,0,1,0.447,1,1V12z"/> <path d="M17,24c0,0.553-0.448,1-1,1H3c-0.552,0-1-0.447-1-1v-9c0-0.553,0.448-1,1-1h13c0.552,0,1,0.447,1,1V24z"/> <path d="M33,24c0,0.553-0.448,1-1,1H19c-0.552,0-1-0.447-1-1v-9c0-0.553,0.448-1,1-1h13c0.552,0,1,0.447,1,1V24z"/> <path d="M49,24c0,0.553-0.448,1-1,1H35c-0.552,0-1-0.447-1-1v-9c0-0.553,0.448-1,1-1h13c0.552,0,1,0.447,1,1V24z"/> </svg><div class="gjs-block-label">More Object</div>' ,
   category: c.category,
   type:'6object',
   content:{type:'6object'}
    // attributes: {class:'gjs-fonts gjs-f-image'},
//     content:  `<div ${attrsRow}> <div ${attrsCell}><div ${attrsRow}>
//     <div ${attrsCell}></div>
//   </div><div ${attrsRow}>
//   <div ${attrsCell}></div>
// </div></div>
//     <div ${attrsCell}><div ${attrsRow}>
//     <div ${attrsCell}></div>
//   </div><div ${attrsRow}>
//   <div ${attrsCell}></div>
// </div></div>
//     <div ${attrsCell}><div ${attrsRow}>
//     <div ${attrsCell}></div>
//   </div><div ${attrsRow}>
//   <div ${attrsCell}></div>
// </div></div>
//   </div>
//   ${ basicStyle ?
//     `<style>
//       ${styleRow}
//       ${styleClm}
//     </style>`
//     : ''}`
  });

//   toAdd('object8') && bm.add('object8', {
//    label:'<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="51px" height="27.003px" viewBox="0 0 51 27.003" enable-background="new 0 0 51 27.003" xml:space="preserve"> <path fill="#FFFFFF" stroke="#000000" stroke-miterlimit="10" d="M50.5,24.152c0,1.297-1.051,2.348-2.348,2.348H2.848 c-1.296,0-2.348-1.051-2.348-2.348V2.848C0.5,1.551,1.551,0.5,2.848,0.5h45.305c1.296,0,2.348,1.051,2.348,2.348V24.152z"/> <path d="M13,12c0,0.553-0.448,1-1,1H3c-0.552,0-1-0.447-1-1V3c0-0.553,0.448-1,1-1h9c0.552,0,1,0.447,1,1V12z"/> <path d="M13,24c0,0.553-0.448,1-1,1H3c-0.552,0-1-0.447-1-1v-9c0-0.553,0.448-1,1-1h9c0.552,0,1,0.447,1,1V24z"/> <path d="M25,12c0,0.553-0.448,1-1,1h-9c-0.552,0-1-0.447-1-1V3c0-0.553,0.448-1,1-1h9c0.552,0,1,0.447,1,1V12z"/> <path d="M25,24c0,0.553-0.448,1-1,1h-9c-0.552,0-1-0.447-1-1v-9c0-0.553,0.448-1,1-1h9c0.552,0,1,0.447,1,1V24z"/> <path d="M37,12c0,0.553-0.448,1-1,1h-9c-0.552,0-1-0.447-1-1V3c0-0.553,0.448-1,1-1h9c0.552,0,1,0.447,1,1V12z"/> <path d="M37,24c0,0.553-0.448,1-1,1h-9c-0.552,0-1-0.447-1-1v-9c0-0.553,0.448-1,1-1h9c0.552,0,1,0.447,1,1V24z"/> <path d="M49,12c0,0.553-0.448,1-1,1h-9c-0.552,0-1-0.447-1-1V3c0-0.553,0.448-1,1-1h9c0.552,0,1,0.447,1,1V12z"/> <path d="M49,24c0,0.553-0.448,1-1,1h-9c-0.552,0-1-0.447-1-1v-9c0-0.553,0.448-1,1-1h9c0.552,0,1,0.447,1,1V24z"/> </svg><div class="gjs-block-label">8 Object</div>', 
//    category: c.category,
//     content: `<div ${attrsRow}> <div ${attrsCell}><div ${attrsRow}>
//     <div ${attrsCell}></div>
//   </div><div ${attrsRow}>
//   <div ${attrsCell}></div>
// </div></div><div ${attrsCell}><div ${attrsRow}>
// <div ${attrsCell}></div>
// </div><div ${attrsRow}>
// <div ${attrsCell}></div>
// </div></div>
//     <div ${attrsCell}><div ${attrsRow}>
//     <div ${attrsCell}></div>
//   </div><div ${attrsRow}>
//   <div ${attrsCell}></div>
// </div></div>
//     <div ${attrsCell}><div ${attrsRow}>
//     <div ${attrsCell}></div>
//   </div><div ${attrsRow}>
//   <div ${attrsCell}></div>
// </div></div>
//   </div>
//   ${ basicStyle ?
//     `<style>
//       ${styleRow}
//       ${styleClm}
//     </style>`
//     : ''}`
//   });

//   toAdd('object10') && bm.add('object10', {
//     label:'<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="53px" height="27.003px" viewBox="0 0 53 27.003" enable-background="new 0 0 53 27.003" xml:space="preserve"> <path fill="#FFFFFF" stroke="#000000" stroke-miterlimit="10" d="M52.5,25.5c0,0.553-0.448,1-1,1h-50c-0.552,0-1-0.447-1-1v-24 c0-0.553,0.448-1,1-1h50c0.552,0,1,0.447,1,1V25.5z"/> <path d="M11,12c0,0.553-0.448,1-1,1H3c-0.552,0-1-0.447-1-1V3c0-0.553,0.448-1,1-1h7c0.552,0,1,0.447,1,1V12z"/> <path d="M21,12c0,0.553-0.448,1-1,1h-7c-0.552,0-1-0.447-1-1V3c0-0.553,0.448-1,1-1h7c0.552,0,1,0.447,1,1V12z"/> <path d="M31,12c0,0.553-0.448,1-1,1h-7c-0.552,0-1-0.447-1-1V3c0-0.553,0.448-1,1-1h7c0.552,0,1,0.447,1,1V12z"/> <path d="M41,12c0,0.553-0.448,1-1,1h-7c-0.552,0-1-0.447-1-1V3c0-0.553,0.448-1,1-1h7c0.552,0,1,0.447,1,1V12z"/> <path d="M51,12c0,0.553-0.448,1-1,1h-7c-0.552,0-1-0.447-1-1V3c0-0.553,0.448-1,1-1h7c0.552,0,1,0.447,1,1V12z"/> <path d="M11,24c0,0.553-0.448,1-1,1H3c-0.552,0-1-0.447-1-1v-9c0-0.553,0.448-1,1-1h7c0.552,0,1,0.447,1,1V24z"/> <path d="M21,24c0,0.553-0.448,1-1,1h-7c-0.552,0-1-0.447-1-1v-9c0-0.553,0.448-1,1-1h7c0.552,0,1,0.447,1,1V24z"/> <path d="M31,24c0,0.553-0.448,1-1,1h-7c-0.552,0-1-0.447-1-1v-9c0-0.553,0.448-1,1-1h7c0.552,0,1,0.447,1,1V24z"/> <path d="M41,24c0,0.553-0.448,1-1,1h-7c-0.552,0-1-0.447-1-1v-9c0-0.553,0.448-1,1-1h7c0.552,0,1,0.447,1,1V24z"/> <path d="M51,24c0,0.553-0.448,1-1,1h-7c-0.552,0-1-0.447-1-1v-9c0-0.553,0.448-1,1-1h7c0.552,0,1,0.447,1,1V24z"/> </svg><div class="gjs-block-label">10 Object</div>',
//     category: c.category,
//     content:`<div ${attrsRow}> <div ${attrsCell}><div ${attrsRow}>
//     <div ${attrsCell}></div>
//   </div><div ${attrsRow}>
//   <div ${attrsCell}></div>
// </div></div><div ${attrsCell}><div ${attrsRow}>
// <div ${attrsCell}></div>
// </div><div ${attrsRow}>
// <div ${attrsCell}></div>
// </div></div><div ${attrsCell}><div ${attrsRow}>
// <div ${attrsCell}></div>
// </div><div ${attrsRow}>
// <div ${attrsCell}></div>
// </div></div>
//     <div ${attrsCell}><div ${attrsRow}>
//     <div ${attrsCell}></div>
//   </div><div ${attrsRow}>
//   <div ${attrsCell}></div>
// </div></div>
//     <div ${attrsCell}><div ${attrsRow}>
//     <div ${attrsCell}></div>
//   </div><div ${attrsRow}>
//   <div ${attrsCell}></div>
// </div></div>
//   </div>
//   ${ basicStyle ?
//     `<style>
//       ${styleRow}
//       ${styleClm}
//     </style>`
//     : ''}`
//   });
}
