// export default (editor, config = {}) => {
//   const bm = editor.BlockManager;
//   const accordionsBlock = config.accordionsBlock;
//   const style = config.style;
//   const type = 'accordions';
//   const content = `<div data-gjs-type="${type}"></div>
//     ${style ? `<style>${style}</style>` : ''}`;

//   accordionsBlock && bm.add(type, {
//     label: 'Accordions Menu',
//     attributes: { class: 'fa fa-arrow-circle-down' },
//     content,
//     ...accordionsBlock
//   });
// }

import React, { Component } from 'react';  
import './LayoutObject.css'
import img1 from './tab1.svg' ;
import img2 from './tab2.svg' ;
import img3 from './tab3.svg' ;
import img4 from './tab4.svg' ;

class EmptyTab extends Component { 

 drawDesgin= (e) =>
  {
    var boxId=e.target.id;
    if(boxId==="item1")
   {
    this.props.designData('<div data-gjs-type="tabs"></div><style>.tab{text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;color:inherit;padding-top:7px;padding-right:14px;padding-bottom:7px;padding-left:14px;transition-duration:.3s;transition-timing-function:ease;transition-delay:0s;transition-property:opacity;display:inline-block;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;margin-right:10px}.tab.tab-active{background-color:rgb(13, 148, 230);color:white;}.tab-content{padding-top:6px;padding-right:12px;padding-bottom:6px;padding-left:12px;min-height:100px;animation-duration:1s;animation-timing-function:ease;animation-delay:0s;animation-iteration-count:1;animation-direction:normal;animation-fill-mode:none;animation-play-state:running;animation-name:fadeEffect}@keyframes fadeEffect{0%{opacity:0;}100%{opacity:1;}}</style>');
   }
   if(boxId==="item2")
   {
    this.props.designData('<div data-gjs-type="tabs"></div><style>.tab{text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;color:inherit;padding-top:7px;padding-right:14px;padding-bottom:7px;padding-left:14px;transition-duration:.3s;transition-timing-function:ease;transition-delay:0s;transition-property:opacity;display:inline-block;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;margin-right:10px}.tab.tab-active{background-color:rgb(13, 148, 230);color:white;}.tab-content{padding-top:6px;padding-right:12px;padding-bottom:6px;padding-left:12px;min-height:100px;animation-duration:1s;animation-timing-function:ease;animation-delay:0s;animation-iteration-count:1;animation-direction:normal;animation-fill-mode:none;animation-play-state:running;animation-name:fadeEffect}@keyframes fadeEffect{0%{opacity:0;}100%{opacity:1;}}</style>');
   }
   if(boxId==="item3")
   {
    this.props.designData('<div data-gjs-type="tabs"></div><style>.tab{text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;color:inherit;padding-top:7px;padding-right:14px;padding-bottom:7px;padding-left:14px;transition-duration:.3s;transition-timing-function:ease;transition-delay:0s;transition-property:opacity;display:inline-block;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;margin-right:10px}.tab.tab-active{background-color:rgb(13, 148, 230);color:white;}.tab-content{padding-top:6px;padding-right:12px;padding-bottom:6px;padding-left:12px;min-height:100px;animation-duration:1s;animation-timing-function:ease;animation-delay:0s;animation-iteration-count:1;animation-direction:normal;animation-fill-mode:none;animation-play-state:running;animation-name:fadeEffect}@keyframes fadeEffect{0%{opacity:0;}100%{opacity:1;}}</style>');
   }
   if(boxId==="item4")
   {
    this.props.designData('<div data-gjs-type="tabs"></div><style>.tab{text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;color:inherit;padding-top:7px;padding-right:14px;padding-bottom:7px;padding-left:14px;transition-duration:.3s;transition-timing-function:ease;transition-delay:0s;transition-property:opacity;display:inline-block;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;margin-right:10px}.tab.tab-active{background-color:rgb(13, 148, 230);color:white;}.tab-content{padding-top:6px;padding-right:12px;padding-bottom:6px;padding-left:12px;min-height:100px;animation-duration:1s;animation-timing-function:ease;animation-delay:0s;animation-iteration-count:1;animation-direction:normal;animation-fill-mode:none;animation-play-state:running;animation-name:fadeEffect}@keyframes fadeEffect{0%{opacity:0;}100%{opacity:1;}}</style>');
   }
      this.props.onClose(false);
  }
  render() 
  {  
    return(
       <>
<div className="row" style={{display:"flex"}}>
    <div className="Layout2">
    <div className="heading-penal">
      <h3 className="margin0" style={{textAlign: "left"}}>{"Horizontal Button"}</h3>
    </div>
    <div className="card"> 
    { <img src={img1} alt="React Logo" />}
    </div>
    <div className="d-flex"> 
    </div><p style={{marginLeft:"10px", fontSize:"14px"}}>Content Type: Tabs</p>
    <div style={{margin:"10px"}}>
    <button className="btn-select" onClick={this.drawDesgin.bind(this)} id="item1">Select</button>
    </div></div>

    <div className="Layout2">
    <div className="heading-penal">
      <h3 className="margin0" style={{textAlign: "left"}}>{"Vertical Button"}</h3>
    </div>
    <div className="card"> 
    { <img src={img2} alt="React Logo" style={{paddingTop:'23px',paddingBottom:'23px'}} />}
    </div>
    <div className="d-flex"> 
    </div><p style={{marginLeft:"10px", fontSize:"14px"}}>Content Type: Tabs</p>
    <div style={{margin:"10px"}}>
    <button className="btn-select" onClick={ this.drawDesgin.bind(this)} id="item2">Select</button>
    </div></div>

</div>
<div className="row" style={{display:"flex"}}>
    <div className="Layout2">
    <div className="heading-penal">
      <h3 className="margin0" style={{textAlign: "left"}}>{"Horizontal Link"}</h3>
    </div>
    <div className="card"> 
    { <img src={img3} alt="React Logo" />}
    </div>
    <div className="d-flex"> 
    </div><p style={{marginLeft:"10px", fontSize:"14px"}}>Content Type: Tabs</p>
    <div style={{margin:"10px"}}>
    <button className="btn-select" onClick={this.drawDesgin.bind(this)} id="item3">Select</button>
    </div></div>

    <div className="Layout2">
    <div className="heading-penal">
      <h3 className="margin0" style={{textAlign: "left"}}>{"Vertical Button"}</h3>
    </div>
    <div className="card"> 
    { <img src={img4} alt="React Logo" style={{paddingTop:'23px',paddingBottom:'23px'}} />}
   
    </div>
    <div className="d-flex"> 
    </div><p style={{marginLeft:"10px", fontSize:"14px"}}>Content Type: Tabs</p>
    <div style={{margin:"10px"}}>
    <button className="btn-select" onClick={this.drawDesgin.bind(this)} id="item4">Select</button>
    </div></div>
</div>
</>
     );
  }  
}  
export default EmptyTab;