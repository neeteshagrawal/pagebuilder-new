import React, { Component } from 'react';  
import './LayoutObject.css'

// const step = 0.2;
//   const minDim = 1;
//   const currentUnit = 1;
//   const resizerBtm = { tl: 1, tc: 1, tr: 1, cl:1, cr:1, bl:1, br: 1, minDim };
//   const resizerRight = { ...resizerBtm, cr: 1, bc: 1, currentUnit, minDim, step };
  
class Object2 extends Component { 
 drawDesgin= (e) =>
  {
    var boxId=e.target.id;
   if(boxId==="item1")
   {
    this.props.designData('<div data-gjs-type="default" draggable="true" data-highlightable="1" class="row gjs gjs-row"><div data-gjs-type="default" draggable="true" data-highlightable="1" class="cell gjs-cell"></div><div data-gjs-type="default" draggable="true" data-highlightable="1" class="cell gjs-cell"></div></div><style>.cell{min-height:75px;flex-grow:1;flex-basis:100%;}.row{display:flex;justify-content:flex-start;align-items:stretch;flex-wrap:nowrap;padding:10px;}@media (max-width: 768px){.foot-form-cont{width:400px;}}</style><style>@media (max-width: 768px){.foot-form-title{width:autopx;}}</style><style>@media (max-width: 768px){.row{flex-wrap:wrap;}}</style>');
   }
   if(boxId==="item2")
   {
    this.props.designData('<div data-gjs-type="default" draggable="true" data-highlightable="1" class="row gjs gjs-row"> <div data-gjs-type="default" draggable="true" data-highlightable="1" class="cell gjs-cell"> <div data-gjs-type="default" draggable="true" data-highlightable="1" class="row gjs gjs-row"> <div data-gjs-type="default" draggable="true" data-highlightable="1" class="cell gjs-cell"> </div></div><div data-gjs-type="default" draggable="true" data-highlightable="1" class="row gjs gjs-row"> <div data-gjs-type="default" draggable="true" data-highlightable="1" class="cell gjs-cell"> </div></div></div></div><style>.cell{min-height:75px;flex-grow:1;flex-basis:100%;}.row{display:flex;justify-content:flex-start;align-items:stretch;flex-wrap:nowrap;padding:10px;}@media (max-width: 768px){.foot-form-cont{width:400px;}}</style><style>@media (max-width: 768px){.foot-form-title{width:autopx;}}</style><style>@media (max-width: 768px){.row{flex-wrap:wrap;}}</style>');
   }
   if(boxId==="item3")
   {
    this.props.designData('<div data-gjs-type="default" draggable="true" data-highlightable="1" class="row gjs-row"><div data-gjs-type="default" draggable="true" data-highlightable="1" class="cell gjs-cell" style="flex-basis:30%;"></div><div data-gjs-type="default" draggable="true" data-highlightable="1" class="cell  gjs-cell" style="flex-basis:70%;"></div></div><style>.cell{min-height:75px;flex-grow:1;flex-basis:100%;}.row{display:flex;justify-content:flex-start;align-items:stretch;flex-wrap:nowrap;padding:10px;}@media (max-width: 768px){.foot-form-cont{width:400px;}}</style><style>@media (max-width: 768px){.foot-form-title{width:autopx;}}</style><style>@media (max-width: 768px){.row{flex-wrap:wrap;}}</style>');
   }
   if(boxId==="item4")
   {
    this.props.designData('<div data-gjs-type="default" draggable="true" data-highlightable="1" class="row gjs-row"><div data-gjs-type="default" draggable="true" data-highlightable="1" class="cell gjs-cell" style="flex-basis:70%;"></div><div data-gjs-type="default" draggable="true" data-highlightable="1" class="cell gjs-cell" style="flex-basis:30%;"></div></div><style>.cell{min-height:75px;flex-grow:1;flex-basis:100%;}.row{display:flex;justify-content:flex-start;align-items:stretch;flex-wrap:nowrap;padding:10px;}@media (max-width: 768px){.foot-form-cont{width:400px;}}</style><style>@media (max-width: 768px){.foot-form-title{width:autopx;}}</style><style>@media (max-width: 768px){.row{flex-wrap:wrap;}}</style>');
   }
      this.props.onClose(false);
  }
  render() 
  {  
    return(
       <>
<div className="row" style={{display:"flex"}}>
    <div className="Layout2">
    <div className="heading-penal">
      <h3 className="margin0" style={{textAlign: "left"}}>{"2 Column"}</h3>
    </div>
    <div className="card"> 
    <div className="l-card-Preview">
    <div className="l-left-Preview"></div>
    <div className="l-right-Preview"></div>
    </div>
    </div>
    <div className="d-flex"> 
    </div><p style={{marginLeft:"10px", fontSize:"14px"}}>Content Type: 2 Object</p>
    <div style={{margin:"10px"}}>
    <button className="btn-select" onClick={this.drawDesgin.bind(this)} id="item1">Select</button>
    </div></div>

    <div className="Layout2">
    <div className="heading-penal">
      <h3 className="margin0" style={{textAlign: "left"}}>{"2 Row"}</h3>
    </div>
    <div className="card"> 
    <div className="l-card-Preview">
    <div className="l-top-Preview"></div>
    <div className="l-bottom-Preview"></div>
    </div>
    </div>
    <div className="d-flex"> 
    </div><p style={{marginLeft:"10px", fontSize:"14px"}}>Content Type: 2 Object</p>
    <div style={{margin:"10px"}}>
    <button className="btn-select" onClick={ this.drawDesgin.bind(this)} id="item2">Select</button>
    </div></div>
</div>
<div className="row" style={{display:"flex"}}>
    <div className="Layout2">
    <div className="heading-penal">
      <h3 className="margin0" style={{textAlign: "left"}}>{"25% / 75%"}</h3>
    </div>
    <div className="card"> 
    <div className="l-card-Preview">
    <div className="l-left-Preview" style={{width:'24%'}}></div>
    <div className="l-right-Preview"  style={{width:'74%'}}></div>
    </div>
    </div>
    <div className="d-flex"> 
    </div><p style={{marginLeft:"10px", fontSize:"14px"}}>Content Type: 2 Object</p>
    <div style={{margin:"10px"}}>
    <button className="btn-select" onClick={this.drawDesgin.bind(this)} id="item3">Select</button>
    </div></div>
    <div className="Layout2">
    <div className="heading-penal">
      <h3 className="margin0" style={{textAlign: "left"}}>{"75% / 25%"}</h3>
    </div>
    <div className="card"> 
    <div className="l-card-Preview">
    <div className="l-left-Preview" style={{width:'74%'}}></div>
    <div className="l-right-Preview" style={{width:'24%'}}></div>
    </div>
    </div>
    <div className="d-flex"> 
    </div><p style={{marginLeft:"10px", fontSize:"14px"}}>Content Type: 2 Object</p>
    <div style={{margin:"10px"}}>
    <button className="btn-select" onClick={this.drawDesgin.bind(this)} id="item4">Select</button>
    </div></div>
</div>
</>
     );
  }  
}  
export default Object2;