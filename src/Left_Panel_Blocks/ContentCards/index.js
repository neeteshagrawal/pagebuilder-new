import grapesjs from 'grapesjs';
//export default function addgjsleftpanelcomponent(editor) {

//funciton addgjsleftpanelcomponent1(editor)
//{
  export default grapesjs.plugins.add("gjs-left-panel-component-content-cards", (editor, opts = {}) => {
//export default grapesjs.plugins.add('gjs-left-panel-component', (editor, opts = {}) => {
  const config = {
    blocks: ['banner', 'category', 'product', 'filtersorting', 'brand', 'popup', 'timer', 'video', 'annoucementbar',
    'map','navigation','newsbar','ordertracker','cart','abandonedcart','ordersummary','shippingmethod','paymentmethod','paymentmethod','progressbar','formbuilder'],
    flexGrid: 0,
    stylePrefix: 'gjs-',
    addBasicStyle: true,
    category: 'Content Cards',
    labelBanner: 'Banner',
    labelCategory: 'Category',
    labelProduct: 'Product',
    labelFilterSorting: 'Filter & Sorting',
    labelBrand: 'Brand',
    labelPopup: 'Popup',
    labelTimer: 'Timer',
    labelVideo: 'Video',
    labelAnnouncementBar: 'Announcement Bar',
    labelMap: 'Map',
    labelNavigation: 'Navigation',
    labelNewsBar: 'News Bar',
    labelOrderTracker: 'Order Tracker',
    labelCart: 'Cart',
    labelAbandonedCart: 'Abandoned Cart',
    labelOrderSummary: 'Order Summary',
    labelShippingMethod: 'Shipping Method',
    labelPaymentMethod: 'Payment Method',
    labelProgressBar: 'Progress Bar',
    labelFormBuilder: 'Form Builder',
    ...opts
  }
//console.log(editor);
  // Add blocks
  const loadBlocks = require('./blocks');
  loadBlocks.default(editor, config);
});
//};
