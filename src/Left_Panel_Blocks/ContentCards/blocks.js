export default function (editor, opt = {}) {
const c = opt;
let bm = editor.BlockManager;
let blocks = c.blocks;
let stylePrefix = c.stylePrefix;
const flexGrid = c.flexGrid;
const basicStyle = c.addBasicStyle;
const clsRow = `${stylePrefix}row`;
const clsCell = `${stylePrefix}cell`;
const styleRow = flexGrid ? `
.${clsRow} {
  display: flex;
  justify-content: flex-start;
  align-items: stretch;
  flex-wrap: nowrap;
  padding: 10px;
}
@media (max-width: 768px) {
.${clsRow} {
flex-wrap: wrap;
}
}` : `
.${clsRow} {
display: table;
padding: 10px;
width: 100%;
}
@media (max-width: 768px) {
.${stylePrefix}cell, .${stylePrefix}cell30, .${stylePrefix}cell70 {
width: 100%;
display: block;
}
}`;
const styleClm = flexGrid ? `
.${clsCell} {
min-height: 75px;
flex-grow: 1;
flex-basis: 100%;
}` : `
.${clsCell} {
width: 8%;
display: table-cell;
height: 75px;
}`;
const styleClm30 = `
.${stylePrefix}cell30 {
width: 30%;
}`;
const styleClm70 = `
.${stylePrefix}cell70 {
width: 70%;
}`;

const step = 0.2;
const minDim = 1;
const currentUnit = 1;
const resizerBtm = { tl: 0, tc: 0, tr: 0, cl: 0, cr:0, bl:0, br: 0, minDim };
const resizerRight = { ...resizerBtm, cr: 1, bc: 0, currentUnit, minDim, step };

// Flex elements do not react on width style change therefore I use
// 'flex-basis' as keyWidth for the resizer on columns
if (flexGrid) {
resizerRight.keyWidth = 'flex-basis';
}

const rowAttr = {
class: clsRow,
'data-gjs-droppable': `.${clsCell}`,
'data-gjs-resizable': resizerBtm,
'data-gjs-name': 'Row',
};

const colAttr = {
class: clsCell,
'data-gjs-draggable': `.${clsRow}`,
'data-gjs-resizable': resizerRight,
'data-gjs-name': 'Cell',
};

if (flexGrid) {
colAttr['data-gjs-unstylable'] = ['width'];
colAttr['data-gjs-stylable-require'] = ['flex-basis'];
}

// Make row and column classes private
const privateCls = [`.${clsRow}`, `.${clsCell}`];
editor.on('selector:add', selector =>
privateCls.indexOf(selector.getFullName()) >= 0 && selector.set('private', 1))

const attrsToString = attrs => {
const result = [];

for (let key in attrs) {
let value = attrs[key];
const toParse = value instanceof Array || value instanceof Object;
value = toParse ? JSON.stringify(value) : value;
result.push(`${key}=${toParse ? `'${value}'` : `"${value}"`}`);
}

return result.length ? ` ${result.join(' ')}` : '';
}

const toAdd = name => blocks.indexOf(name) >= 0;
const attrsRow = attrsToString(rowAttr);
const attrsCell = attrsToString(colAttr);

toAdd('banner') && bm.add('banner', {
  label: c.labelBanner,
  category: c.category,
  type:'banner',
  attributes: {class:'fa fa-image'},
  content:{type:'banner'}
});
toAdd('category') && bm.add('category', {
  label: c.labelCategory,
  category: c.category,
  attributes: {class:'fa fa-folder'},
  type:'category',
  content:{type:'category'}
});

toAdd('product') && bm.add('product', {
  label: c.labelProduct,
  attributes: {class:'fa fa-dropbox'},
  category: c.category,
  type:'product',
  content:{type:'product'}
});

toAdd('filtersorting') && bm.add('filtersorting', {
  label: c.labelFilterSorting,
  category: c.category,
  attributes: {class:'fa fa-filter'},
  content: `<div ${attrsRow}>
  <div ${attrsCell}></div>
  <div ${attrsCell}></div>
  <div ${attrsCell}></div>
  </div>
  ${ basicStyle ?
  `<style>
  ${styleRow}
  ${styleClm}
  </style>`
  : ''}`
});

toAdd('brand') && bm.add('brand', {
  label: c.labelBrand,
  category: c.category,
  attributes: {class:'fa fa-first-order'},
  content: `<div ${attrsRow}>
  <div ${attrsCell} style="${flexGrid ? 'flex-basis' : 'width'}: 30%;"></div>
  <div ${attrsCell} style="${flexGrid ? 'flex-basis' : 'width'}: 70%;"></div>
  </div>
  ${ basicStyle ?
  `<style>
  ${styleRow}
  ${styleClm}
  ${styleClm30}
  ${styleClm70}
  </style>`
  : ''}`
});

toAdd('popup') && bm.add('popup', {
  label: c.labelPopup,
  category: c.category,
  attributes: {class:'fa fa-window-maximize'},
  content: {
  type:'text',
  content:'Insert your text here',
  style: {padding: '10px' },
  activeOnRender: 1
  },
});
toAdd('product') && bm.add('product', {
  label: c.labelProduct,
  attributes: {class:'fa fa-dropbox'},
  category: c.category,
  type:'product',
  content:{type:'product'}
});
toAdd('timer') && bm.add('timer', {
  label: c.labelTimer,
  category: c.category,
  attributes: {class:'fa fa-user'},
  type:'timer',
  content: {type:'timer'},
});

toAdd('video') && bm.add('video', {
  label: c.labelVideo,
  category: c.category,
  type:'video',
  attributes: {class:'fa fa-video-camera'},
  content: {type:'video'},
});

toAdd('announcementbar') && bm.add('announcementbar', {
  label: c.labelAnnouncementBar,
  category: c.category,
  attributes: {class:'fa fa-comments'},
  content:''

});
toAdd('map') && bm.add('map', {
  label: c.labelMap,
  category: c.category,
  type:'map',
  attributes: {class:'fa fa-map-marker'},
  content:{type:'map'}
});
toAdd('navigation') && bm.add('navigation', {
  label: c.labelNavigation,
  category: c.category,
  type:'navigation',
  attributes: {class:'fa fa-bars'},
  content:{type:'navigation'}
});
toAdd('newsbar') && bm.add('newsbar', {
  label: c.labelNewsBar,
  category: c.category,
  type:'newsbar',
  attributes: {class:'fa fa-newspaper-o'},
  content:{type:'newsbar'}
});
toAdd('ordertracker') && bm.add('ordertracker', {
  label: c.labelOrderTracker,
  category: c.category,
  attributes: {class:'fa fa-wpexplorer'},
  content: {
  type:'link',
  content:'Link',
  style: {color: '#d983a6'}
  },
});
toAdd('cart') && bm.add('cart', {
  label: c.labelCart,
  category: c.category,
  attributes: {class:'fa fa-shopping-cart'},
  content: {
  type:'link',
  content:'Link',
  style: {color: '#d983a6'}
  },
});
toAdd('abandonedcart') && bm.add('abandonedcart', {
  label: c.labelAbandonedCart,
  category: c.category,
  attributes: {class:'fa fa-cart-arrow-down'},
  content: {
  type:'link',
  content:'Link',
  style: {color: '#d983a6'}
  },
});
toAdd('ordersummary') && bm.add('ordersummary', {
  label: c.labelOrderSummary,
  category: c.category,
  attributes: {class:'fa fa-wpforms'},
  content: {
  type:'link',
  content:'Link',
  style: {color: '#d983a6'}
  },
});
toAdd('shippingmethod') && bm.add('shippingmethod', {
  label: c.labelShippingMethod,
  category: c.category,
  attributes: {class:'fa fa-truck'},
  content: {
  type:'link',
  content:'Link',
  style: {color: '#d983a6'}
  },
});
toAdd('paymentmethod') && bm.add('paymentmethod', {
  label: c.labelPaymentMethod,
  category: c.category,
  attributes: {class:'fa fa-credit-card'},
  content: {
  type:'link',
  content:'Link',
  style: {color: '#d983a6'}
  },
});
toAdd('progressbar') && bm.add('progressbar', {
  label: c.labelProgressBar,
  category: c.category,
  attributes: {class:'fa fa-forward'},
  content: {
  type:'link',
  content:'Link',
  style: {color: '#d983a6'}
  },
});
toAdd('formbuilder') && bm.add('formbuilder', {
  label: c.labelFormBuilder,
  category: c.category,
  attributes: {class:'fa fa-edit'},
  content: {
  type:'link',
  content:'Link',
  style: {color: '#d983a6'}
  },
});

}
