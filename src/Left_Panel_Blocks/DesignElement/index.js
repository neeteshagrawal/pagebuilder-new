import grapesjs from 'grapesjs';
//export default function addgjsleftpanelcomponent(editor) {

//funciton addgjsleftpanelcomponent1(editor)
//{
  export default grapesjs.plugins.add("gjs-left-panel-component-design-element", (editor, opts = {}) => {

//export default grapesjs.plugins.add('gjs-left-panel-component', (editor, opts = {}) => {
  const config = {
    blocks: ['text', 'heading', 'button', 'divider', 'tabCard','htmlcode', 'accordion', 'searchbar', 'megamenu'],
    flexGrid: 0,
    stylePrefix: 'gjs-',
    addBasicStyle: true,
    category: 'Design Element',
    labelText: 'Text',
    labelHeading: 'Heading',
    labelButton: 'Button',
    labelDivider: 'Divider',
    labelTab: 'Tab',
    labelHTMLCode: 'HTML Code',
    labelAccordion: 'Accordion',
    labelSearchBar: 'Search Bar',
    labelMegaMenu: 'Mega Menu',
    ...opts
  }
//console.log(editor);
  // Add blocks
  const loadBlocks = require('./blocks');
  loadBlocks.default(editor, config);
});
//};
