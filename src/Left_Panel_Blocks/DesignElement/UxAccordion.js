import React, { Component } from 'react';  
import { ChromePicker } from "react-color";
import Select from "react-select";
import makeAnimated from "react-select/animated";
//import './LayoutObject.css'
import img1 from './Acc1.svg' ;
import img2 from './Acc2.svg' ;
import img3 from './Acc3.svg' ;



const animatedComponents = makeAnimated();
const options = [
  { value: "101", label: "Segment1" },
  { value: "102", label: "Segment2" },
  { value: "103", label: "Segment3" },
];

const MyComponent = () => (
  <Select
    /*styles={customStyles}*/ options={options}
    closeMenuOnSelect={true}
    components={animatedComponents}
    /*defaultValue={[options[1], options[2]]}*/ isMulti
  />
);
class UxAccordion extends Component { 
  constructor(props) {
    super(props);
    this.state = {
      isNormalLocation: true,
      isPersonalizedLocation: false,
      isSetting: true,
      isDividerTemplate:true,
      istoggleFirstHeading: true,
      isLayout: true
    };
  }
 drawDesgin= (e) =>
  {
    var boxId=e.target.id;
    if(boxId==="item1")
   {
    this.props.designData('<div data-gjs-type="accordions"></div><style>.accordion{text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;color:inherit;padding-top:7px;padding-right:14px;padding-bottom:7px;padding-left:14px;transition-duration:.3s;transition-timing-function:ease;transition-delay:0s;transition-property:opacity;display:block;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;margin-right:10px;background-color:#eee;margin-top:5px}.accordion-content{display:none;padding-top:6px;padding-right:12px;padding-bottom:6px;padding-left:12px;min-height:100px;border-top-width:1px;border-right-width:1px;border-bottom-width:1px;border-left-width:1px;border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-top-color:#eee;border-right-color:#eee;border-bottom-color:#eee;border-left-color:#eee;border-image-source:initial;border-image-slice:initial;border-image-width:initial;border-image-outset:initial;border-image-repeat:initial}</style>');
   }
   if(boxId==="item2")
   {
    this.props.designData('<div data-gjs-type="accordions"></div><style>.accordion{text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;color:inherit;padding-top:7px;padding-right:14px;padding-bottom:7px;padding-left:14px;transition-duration:.3s;transition-timing-function:ease;transition-delay:0s;transition-property:opacity;display:block;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;margin-right:10px;background-color:#eee;margin-top:5px}.accordion-content{display:none;padding-top:6px;padding-right:12px;padding-bottom:6px;padding-left:12px;min-height:100px;border-top-width:1px;border-right-width:1px;border-bottom-width:1px;border-left-width:1px;border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-top-color:#eee;border-right-color:#eee;border-bottom-color:#eee;border-left-color:#eee;border-image-source:initial;border-image-slice:initial;border-image-width:initial;border-image-outset:initial;border-image-repeat:initial}</style>');
   }
   if(boxId==="item3")
   {
    this.props.designData('<div data-gjs-type="accordions"></div><style>.accordion{text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;color:inherit;padding-top:7px;padding-right:14px;padding-bottom:7px;padding-left:14px;transition-duration:.3s;transition-timing-function:ease;transition-delay:0s;transition-property:opacity;display:block;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;margin-right:10px;background-color:#eee;margin-top:5px}.accordion-content{display:none;padding-top:6px;padding-right:12px;padding-bottom:6px;padding-left:12px;min-height:100px;border-top-width:1px;border-right-width:1px;border-bottom-width:1px;border-left-width:1px;border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-top-color:#eee;border-right-color:#eee;border-bottom-color:#eee;border-left-color:#eee;border-image-source:initial;border-image-slice:initial;border-image-width:initial;border-image-outset:initial;border-image-repeat:initial}</style>');
   }
   if(boxId==="item4")
   {
    this.props.designData('<div data-gjs-type="accordions"></div><style>.accordion{text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;color:inherit;padding-top:7px;padding-right:14px;padding-bottom:7px;padding-left:14px;transition-duration:.3s;transition-timing-function:ease;transition-delay:0s;transition-property:opacity;display:block;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;margin-right:10px;background-color:#eee;margin-top:5px}.accordion-content{display:none;padding-top:6px;padding-right:12px;padding-bottom:6px;padding-left:12px;min-height:100px;border-top-width:1px;border-right-width:1px;border-bottom-width:1px;border-left-width:1px;border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-top-color:#eee;border-right-color:#eee;border-bottom-color:#eee;border-left-color:#eee;border-image-source:initial;border-image-slice:initial;border-image-width:initial;border-image-outset:initial;border-image-repeat:initial}</style>');
   }
      this.props.onClose(false);
  }
  showNormalLocation = (e) => {
    this.setState(
      {
        isNormalLocation: true,
        isPersonalizedLocation: false,
      },
      () => {
        console.log(this.state);
      }
    );
  };
  showPersonalizedLocation = (e) => {
    this.setState(
      {
        isNormalLocation: false,
        isPersonalizedLocation: true,
      },
      () => {
        console.log(this.state);
      }
    );
  };
 
  showDividerTemplate = (e) => {
    this.setState(
      {
        isDividerTemplate:false
      },
      () => {
        console.log(this.state);
      }
    );
  };
  upDividerTemplate = (e) => {
    this.setState(
      {
        isDividerTemplate:true
      },
      () => {
        console.log(this.state);
      }
    );
  };
  showSetting = (e) => {
    this.setState({ isSetting: false }, () => {
      console.log(this.state)
    });
  };
  upSetting = (e) => {
    this.setState({ isSetting: true }, () => {
      console.log(this.state)
    });
  };

  toggleFirstHeading = (e) => {
    this.setState({ istoggleFirstHeading: false }, () => {
    });
  };
  uptoggleFirstHeading = (e) => {
    this.setState({ istoggleFirstHeading: true }, () => {
    });
  };
  downLayout = (e) => {
    this.setState({ isLayout: false }, () => {
    });
  };
  upLayout = (e) => {
    this.setState({ isLayout: true }, () => {
    });
  };
  render() 
  {
    var chevronUpImg = (
      <svg
        class="bi bi-chevron-up"
        width="1em"
        height="1em"
        viewBox="0 0 16 16"
        fill="currentColor"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fill-rule="evenodd"
          d="M7.646 4.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1-.708.708L8 5.707l-5.646 5.647a.5.5 0 0 1-.708-.708l6-6z"
        ></path>{" "}
      </svg>
    );
  
    var biImg = <svg class="bi bi-chevron-up" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" d="M7.646 4.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1-.708.708L8 5.707l-5.646 5.647a.5.5 0 0 1-.708-.708l6-6z"></path> </svg>;
    var downImg =
      <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chevron-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
      </svg>
       var tImg = (
        <svg
          class="bi bi-fonts"
          width="1em"
          height="1em"
          viewBox="0 0 16 16"
          fill="#fff"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M12.258 3H3.747l-.082 2.46h.479c.26-1.544.758-1.783 2.693-1.845l.424-.013v7.827c0 .663-.144.82-1.3.923v.52h4.082v-.52c-1.162-.103-1.306-.26-1.306-.923V3.602l.43.013c1.935.062 2.434.301 2.694 1.846h.479L12.258 3z"></path>
        </svg>
      );
    return(
       <>
<div className="banner-conten" >
{this.state.isDividerTemplate === true ? 
<div>
  <div onClick={this.showDividerTemplate}>
            <span class=" float-right" style={{ color: "#fff", fontSize: "20px",paddingTop: "8px",
    marginRight: "10px" }}>
                  {biImg}
                </span>
                <h3 style={{    
    background: "#006cb4",
    color: "#fff",
    fontSize: "14px",
    padding: "8px 6px 8px",
    borderRadius: "6px 6px 0 0"}}>Accordion Template</h3>
    </div>
<div className="row" style={{display:"flex"}}>
    <div className="Layout2">
      
    <div className="heading-penal">
      <h3 className="margin0" style={{textAlign: "left"}}>{"Accordion Op1"}</h3>
    </div>
    <div className="card"> 
    { <img src={img1} alt="React Logo" />}
    </div>
    <div className="d-flex"> 
    <label>Status</label>
      <div className="switch">
        <div className="checkbox switcher">
          <label htmlFor="overlayColor"><input type="checkbox" id="overlayColor" onChange={this.onValueChangeFrame} value="" /><span><small></small></span> </label>
        </div>
        </div><br/>
    </div><p style={{marginLeft:"10px", fontSize:"14px"}}>Content Type: Accordion</p>
    <div style={{margin:"10px"}}>
    <button className="btn-select" id="item1">Select</button>
    </div></div>

    <div className="Layout2">
    <div className="heading-penal">
      <h3 className="margin0" style={{textAlign: "left"}}>{"Accordion Op2"}</h3>
    </div>
    <div className="card"> 
    { <img src={img2} alt="React Logo" />}
    </div>
    <div className="d-flex"> 
    <label>Status</label>
                          <div className="switch">
                            <div className="checkbox switcher">
                              <label htmlFor="overlayColor"><input type="checkbox" id="overlayColor" onChange={this.onValueChangeFrame} value="" /><span><small></small></span> </label>
                            </div>
                            </div><br/>
    </div><p style={{marginLeft:"10px", fontSize:"14px"}}>Content Type: Accordion</p>
    <div style={{margin:"10px"}}>
    <button className="btn-select"  id="item2">Select</button>
    </div></div>

</div>
<div className="row" style={{display:"flex"}}>
    <div className="Layout2">
    <div className="heading-penal">
      <h3 className="margin0" style={{textAlign: "left"}}>{"Accordion Op3"}</h3>
    </div>
    <div className="card"> 
    { <img src={img3} alt="React Logo" />}
    </div>
    <div className="d-flex"> 
    </div>
    <label>Status</label>
                          <div className="switch">
                            <div className="checkbox switcher">
                              <label htmlFor="overlayColor"><input type="checkbox" id="overlayColor" onChange={this.onValueChangeFrame} value="" /><span><small></small></span> </label>
                            </div>
                            </div><br/>
    <p style={{marginLeft:"10px", fontSize:"14px"}}>Content Type: Accordion</p>
    <div style={{margin:"10px"}}>
    <button className="btn-select" id="item3">Select</button>
    </div></div>

    <div className="Layout2" style={{border:0}}>
    </div>
</div>
</div>:
<div className="heading-penal" onClick={this.upDividerTemplate}>
             <span class=" float-right" style={{ color: "#fff", fontSize: "20px" }}>
               {downImg}
             </span>
             <h3 className="">Accordion Template</h3>
           </div>}
</div>
<br/>
{this.state.isSetting === true ? <div className="banner-conten" >
              <div className="heading-penal" onClick={this.showSetting}>
                <span class=" float-right" style={{ color: "#fff", fontSize: "20px" }}>
                  {biImg}
                </span>
                <h3 className="">Setting</h3>
              </div>
                <div className="card pt-0">
                  <div className="justify-content-around position-relative overflow-hidden ">
                    <span class=" float-right">{chevronUpImg}</span>
                    <h3 className="mt-0"> Accordion Setting</h3>
                 
                  </div>

                  <ul>
                    <li>
                      <strong>
                      Accordion Style
                        <ul>
                          <li>
                            <br />
                            Border/Line:
                            <input
                              id="rs-range-line"
                              class="rs-range w-100"
                              type="range"
                              value="0"
                              min="0"
                              max="100"
                            ></input>
                            <br />
                          </li>
                          <li>
                            <br />
                            Height:
                            <input
                              id="rs-range-line"
                              class="rs-range w-100"
                              type="range"
                              value="0"
                              min="0"
                              max="100"
                            ></input>
                            <br />
                          </li>
                          <li>
                            <br />
                            Width:
                            <input
                              id="rs-range-line"
                              class="rs-range w-100"
                              type="range"
                              value="0"
                              min="0"
                              max="100"
                            ></input>
                            <br />
                          </li>
                          <li>
                            <br />
                            Rounded Edge:
                            <input
                              id="rs-range-line"
                              class="rs-range w-100"
                              type="range"
                              value="0"
                              min="0"
                              max="100"
                            ></input>
                            <br />
                          </li>
                          <li>
                            <br />
                            Accordion Icon:
                            <select name="cars" id="cars" style={{ width: "117px", marginLeft: "13px" }}>  </select>
                            <br />
                          </li>
                        </ul>
                      </strong>
                    </li>
                  </ul>
                  <ul>
                    <li>
                      <strong>
                        Accordion Label
                        <br />
                        <ul>
                          <li>
                            <br />
                            Label:
                            <br />
                            <ul>
                              <li>
                                <br />
                                <label>
                                  <input class="mr-2" type="radio" name="" />
                                  Text
                                </label>
                                <br />
                              </li>
                              <li>
                                <br />
                                <label>
                                  <input class="mr-2" type="radio" name="" />
                                  Icon
                                </label>
                                <br />
                              </li>
                              <li>
                                <br />
                                <label>
                                  <input class="mr-2" type="radio" name="" />
                                  Text & Icon
                                </label>
                                <br />
                              </li>
                            </ul>
                          </li>
                          <li>
                            <br />
                            Label Alignement
                            <br />
                            <ul>
                              <li>
                                <br />
                                <label>
                                  <input class="mr-2" type="radio" name="" />
                                  Center
                                </label>
                                <br />
                              </li>
                              <li>
                                <br />
                                <label>
                                  <input class="mr-2" type="radio" name="" />
                                  Leading
                                </label>
                                <br />
                              </li>
                            </ul>
                          </li>
                          <li>
                            <br />
                            Icon Position
                            <br />
                            <ul>
                              <li>
                                <br />
                                <label>
                                  <input class="mr-2" type="radio" name="" />
                                  Top
                                </label>
                                <br />
                              </li>
                              <li>
                                <br />
                                <label>
                                  <input class="mr-2" type="radio" name="" />
                                  Leading
                                </label>
                                <br />
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </strong>
                    </li>
                  </ul>
                  <br />
                  <ul>
                    <li>
                      <strong>
                        Accordion Color
                        <ul>
                          <li>
                            <br />
                            <div className="d-flex">
                              <span
                                className="slide-conten"
                                style={{ marginRight: "40px" }}
                              >
                                Activ Accordion Color
                              </span>
                              <div className="color-picker-container">
                                <div
                                  className="color-picker-color-background"
                                  style={{
                                    backgroundColor: this.state.changeColor,
                                  }}
                                ></div>
                                <input
                                  type="text"
                                  id="clr-pkr-color"
                                  readOnly
                                  value={this.state.changeColor}
                                  className="color-picker-text"
                                  onClick={this.onHandleShowColorPicker}
                                />
                                {this.state.displayColorPicker && (
                                  <div className="color-picker-palette">
                                    <div
                                      className="color-picker-cover"
                                      onClick={this.handleClose}
                                    >
                                      {" "}
                                    </div>
                                    <ChromePicker
                                      color={this.state.color}
                                      onChange={(color, evt) =>
                                        this.onChangeColorPicker(color, evt)
                                      }
                                    ></ChromePicker>
                                  </div>
                                )}
                              </div>
                            </div>
                          </li>
                          <li>
                            <br />
                           
                            <div className="d-flex">
                              <span
                                className="slide-conten"
                                style={{ marginRight: "40px" }}
                              >
                                Active Label Color
                              </span>
                              <div className="color-picker-container">
                                <div
                                  className="color-picker-color-background"
                                  style={{
                                    backgroundColor: this.state.changeColor,
                                  }}
                                ></div>
                                <input
                                  type="text"
                                  id="clr-pkr-color"
                                  readOnly
                                  value={this.state.changeColor}
                                  className="color-picker-text"
                                  onClick={this.onHandleShowColorPicker}
                                />
                                {this.state.displayColorPicker && (
                                  <div className="color-picker-palette">
                                    <div
                                      className="color-picker-cover"
                                      onClick={this.handleClose}
                                    >
                                      {" "}
                                    </div>
                                    <ChromePicker
                                      color={this.state.color}
                                      onChange={(color, evt) =>
                                        this.onChangeColorPicker(color, evt)
                                      }
                                    ></ChromePicker>
                                  </div>
                                )}
                              </div>
                            </div>
                          </li>
                          <li>
                            <br />
                            <div className="d-flex">
                              <span
                                className="slide-conten"
                                style={{ marginRight: "40px" }}
                              >
                                Detective Accordion Color
                              </span>
                              <div className="color-picker-container">
                                <div
                                  className="color-picker-color-background"
                                  style={{
                                    backgroundColor: this.state.changeColor,
                                  }}
                                ></div>
                                <input
                                  type="text"
                                  id="clr-pkr-color"
                                  readOnly
                                  value={this.state.changeColor}
                                  className="color-picker-text"
                                  onClick={this.onHandleShowColorPicker}
                                />
                                {this.state.displayColorPicker && (
                                  <div className="color-picker-palette">
                                    <div
                                      className="color-picker-cover"
                                      onClick={this.handleClose}
                                    >
                                      {" "}
                                    </div>
                                    <ChromePicker
                                      color={this.state.color}
                                      onChange={(color, evt) =>
                                        this.onChangeColorPicker(color, evt)
                                      }
                                    ></ChromePicker>
                                  </div>
                                )}
                              </div>
                            </div>
                          </li>
                          <li>
                            <br />
                            <div className="d-flex">
                              <span
                                className="slide-conten"
                                style={{ marginRight: "40px" }}
                              >
                                Detective Label Color
                              </span>
                              <div className="color-picker-container">
                                <div
                                  className="color-picker-color-background"
                                  style={{
                                    backgroundColor: this.state.changeColor,
                                  }}
                                ></div>
                                <input
                                  type="text"
                                  id="clr-pkr-color"
                                  readOnly
                                  value={this.state.changeColor}
                                  className="color-picker-text"
                                  onClick={this.onHandleShowColorPicker}
                                />
                                {this.state.displayColorPicker && (
                                  <div className="color-picker-palette">
                                    <div
                                      className="color-picker-cover"
                                      onClick={this.handleClose}
                                    >
                                      {" "}
                                    </div>
                                    <ChromePicker
                                      color={this.state.color}
                                      onChange={(color, evt) =>
                                        this.onChangeColorPicker(color, evt)
                                      }
                                    ></ChromePicker>
                                  </div>
                                )}
                              </div>
                            </div>
                          </li>
                          <li>
                            <br />
                            <div className="d-flex">
                              <span className="slide-conten" style={{ marginRight: "40px" }} >
                                Border Color
                              </span>
                              <div className="color-picker-container">
                                <div
                                  className="color-picker-color-background"
                                  style={{
                                    backgroundColor: this.state.changeColor,
                                  }}
                                ></div>
                                <input
                                  type="text"
                                  id="clr-pkr-color"
                                  readOnly
                                  value={this.state.changeColor}
                                  className="color-picker-text"
                                  onClick={this.onHandleShowColorPicker}
                                />
                                {this.state.displayColorPicker && (
                                  <div className="color-picker-palette">
                                    <div
                                      className="color-picker-cover"
                                      onClick={this.handleClose}
                                    >
                                      {" "}
                                    </div>
                                    <ChromePicker
                                      color={this.state.color}
                                      onChange={(color, evt) =>
                                        this.onChangeColorPicker(color, evt)
                                      }
                                    ></ChromePicker>
                                  </div>
                                )}
                              </div>
                            </div>
                          </li>
                          <li>
                            <br />
                            <div className="d-flex">
                              <span className="slide-conten" style={{ marginRight: "40px" }} >
                               Background Color
                              </span>
                              <div className="color-picker-container">
                                <div className="color-picker-color-background" style={{
                                    backgroundColor: this.state.changeColor, }} ></div>
                                <input
                                  type="text"
                                  id="clr-pkr-color"
                                  readOnly
                                  value={this.state.changeColor}
                                  className="color-picker-text"
                                  onClick={this.onHandleShowColorPicker}
                                />
                                {this.state.displayColorPicker && (
                                  <div className="color-picker-palette">
                                    <div
                                      className="color-picker-cover"
                                      onClick={this.handleClose}
                                    >
                                      {" "}
                                    </div>
                                    <ChromePicker
                                      color={this.state.color}
                                      onChange={(color, evt) =>
                                        this.onChangeColorPicker(color, evt)
                                      }
                                    ></ChromePicker>
                                  </div>
                                )}
                              </div>
                            </div>
                          </li>
                        </ul>
                      </strong>
                    </li>
                  </ul>
                </div>
             </div> : <div className="heading-penal" onClick={this.upSetting}>
             <span class=" float-right" style={{ color: "#fff", fontSize: "20px" }}>
               {downImg}
             </span>
             <h3 className="">Setting</h3>
           </div>
         }

<br/>
<div className="banner-conten">
         {this.state.istoggleFirstHeading === true ? <div>
              <div className="heading-penal" onClick={this.toggleFirstHeading}>
                <span class=" float-right" style={{ color: "#fff", fontSize: "20px" }}>
                  {biImg}
                </span>
                <h3 className="">Accordion List</h3>
              </div>

              <div className="heading-penal">
                <h3 className=""></h3>
              </div>
              <div className="add-btn">
  
                <button className="btn" onClick={this.showNormalLocation}>
                  {" "}
                  + Add New Accordion{" "}
                </button>
                <button className="btn" onClick={this.showPersonalizedLocation}>
                  {" "}
                  + Add Personalized Accordion
                </button>
                
              </div>
              {this.state.isNormalLocation === true ? (
                <div className="card pt-0">
                  <div className="justify-content-around position-relative overflow-hidden ">
                    <h3 className="mt-0">Accordion 1</h3>
                    <div className="personlized"> Normal Accordion</div>
                  </div>

                  <div className="d-flex">
                    {" "}
                    <span
                      className="slide-conten"
                      style={{ marginRight: "10px" }}
                    >
                      Accordion Name&nbsp;&nbsp;&nbsp;
                    </span>
                    <div className="color-picker-container">
                      <input type="text" className="frame-style-input" />
                    </div>
                  </div>
                  <br />
               
                  <div className="banner-conten-device">
                    <div className="d-flex" style={{ width: "850px" }}>
                      <div className="desktop">
                        <div className="heading-penal">
                          <h3 style={{ textAlign: "center" }}>
                          Accordion Content
                          </h3>
                        </div>
                        <div>
                          <div className="add-img flexImg2" >
                            <img src="dist/img1.png" />
                            <input type="text" class="inputContent" />
                            <div class="flexML">
                              <span class="textBackBox">
                                {tImg}
                              </span>
                            </div>
                          </div>
                        </div>
                        <div>
                          <div className="add-img flexImg2">
                            <img src="dist/img1.png" />
                            <input type="text" class="inputContent"/>
                            <div class="flexML" >
                              <span class="textBackBox" >
                                {tImg}
                              </span>
                            </div>
                          </div>
                          <div className="add-img">
                            <div className="add-img flexImg" >
                              <input type="text" class="inputContent"/>
                              <div class="flexML" >
                                <span class="textBackBox">
                                <i class="fas fa-paperclip" aria-hidden="true" style={{ color: "white" }} ></i>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <br/>
                </div>
              ) : (
                ""
              )}
              {this.state.isPersonalizedLocation === true ? (
                 <div className="card pt-0">
                 <div className="justify-content-around position-relative overflow-hidden ">
                   <h3 className="mt-0">Accordion ab2</h3>
                   <div className="personlized"> Personlized Accordion</div>
                 </div>
                 <div className="d-flex">
                   {" "}
                   <span  className="slide-conten" style={{ marginRight: "10px" }} >
                     Accordion Name&nbsp;&nbsp;&nbsp;
                   </span>
                   <div className="color-picker-container">
                     <input type="text" className="frame-style-input" />
                   </div>
                 </div>
                 <br />
                 <div className="d-flex">
                   <span className="slide-conten" style={{ marginRight: "26px" }} >
                     Segment & Tags
                   </span>
                   <div className="color-picker-container">
                     <MyComponent />
                     <p className="mt-0">
                       <small>
                         select the segment of contact that will see the this banner(you can select more than one)
                       </small>
                     </p>
                   </div>
                 </div>
                 <div className="banner-conten-device">
                   <div className="d-flex" style={{ width: "850px" }}>
                     <div className="desktop">
                       <div className="heading-penal">
                         <h3 style={{ textAlign: "center" }}>
                         Accordion Content
                         </h3>
                       </div>
                       <div>
                         <div className="add-img" style={{ display: "flex", flex: "1 1 0%" }} >
                           <img src="dist/img1.png" />
                           <input type="text" class="inputContent"/>
                           <div style={{ marginLeft: "-1px", display: "flex" }} >
                             <span class="textBackBox">{tImg} </span>
                           </div>
                         </div>
                       </div>
                       <div>
                       <div className="add-img" style={{ display: "flex", flex: "1 1 0%" }} >
                           <img src="dist/img1.png" />
                           <input type="text" class="inputContent" />
                           <div  style={{ marginLeft: "-1px", display: "flex" }}>
                             <span class="textBackBox">
                               {tImg}
                             </span>
                           </div>
                         </div>
                         <div className="add-img">
                           <div className="add-img flexImg" >
                             <input type="text" class="inputContent" />
                             <div style={{ marginLeft: "-1px", display: "flex" }} >
                               <span class="textBackBox">
                                 <i class="fas fa-paperclip"  aria-hidden="true"
                                   style={{ color: "white" }}></i>
                               </span>
                             </div>
                           </div>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
                 <br/>

               </div>
              ) : (
                ""
              )}
              
               </div>
              :
              <div className="heading-penal" onClick={this.uptoggleFirstHeading}>
                <span class=" float-right" style={{ color: "#fff", fontSize: "20px" }}>
                  {downImg}
                </span>
                <h3 className="">Accordion List</h3>
              </div>}
            </div>
</>

     );
  }  
}  
export default UxAccordion;