import React,{Component} from 'react';
import 'grapesjs/dist/css/grapes.min.css';
import GrapesJS from 'grapesjs';
import gjsPresetWebpage from 'grapesjs-preset-webpage';
import GrapesjsTabs from 'grapesjs-tabs';
import block_Layout from './Left_Panel_Blocks/Layout/index';
import block_ContentCards from './Left_Panel_Blocks/ContentCards/index';
import block_DesignElement from './Left_Panel_Blocks/DesignElement/index';
import './PageBuilder.css'
import svModel from './Modal_Popups/CardSave';
import Banner from './Modal_Popups/Banner';
import Object2 from './Left_Panel_Blocks/Layout/Object2';
import Object3 from './Left_Panel_Blocks/Layout/Object3';
import Object4 from './Left_Panel_Blocks/Layout/Object4';
import Object5 from './Left_Panel_Blocks/Layout/Object5';
import MoreObject from './Left_Panel_Blocks/Layout/MoreObject';
import EmptyTab from './Left_Panel_Blocks/Layout/EmptyTab';
import UxAccordion from './Left_Panel_Blocks/DesignElement/UxAccordion';
import Accordions from './Accordions_Block/src/index';
import Map from './Modal_Popups/Map';
import HTMLCode from './Left_Panel_Blocks/DesignElement/HTMLCode';
import HeadingCard from './Left_Panel_Blocks/DesignElement/HeadingCard';
import Product from './Modal_Popups/Product';
import Category from './Modal_Popups/Category';
import Timer from './Modal_Popups/Timer';
import Divider from './Left_Panel_Blocks/DesignElement/Divider';
import Navigation from './Modal_Popups/Navigation';
import Video from './Modal_Popups/Video';
import NewsBar from './Modal_Popups/NewsBar';
import Tab from './Left_Panel_Blocks/DesignElement/Tab';
import Text from './Left_Panel_Blocks/DesignElement/Text';

import Dialog from './Modal_Popups/Dialog';
class PageBuilder extends Component {
constructor(props) {
    super(props);
    this.state = {
      isOpenO2: false,
      isOpenO3: false,
      isOpenO4: false,
      isOpenO5: false,
      isOpenObMore: false,
      isOpenBanner: false,
      isOpenEmptyTab: false,
      isOpenUxAccordion: false,
      isOpenMap: false,
      isOpenHTMLCode:false,
      isOpenHeadingCard: false,
      isProduct: false,
      isTimer:false,
      isDivider:false,
      isNavigation:false,
      isVideo:false,
      isNewsBar:false,
      isOpenTab:false,
      isText:false
    }
      this.editorBuilder=null;
      this.selComp=null;
    }
   // state = {isOpen: false,isOpenBanner: false}
    componentDidMount() {
        /*const e*/  this.editorBuilder=  GrapesJS.init({
           avoidInlineStyle: 1, 
          container:'#example-editor',
          fromElement: 1,
          showOffsets: 1,
           selectorManager: { componentFirst: true },
           styleManager: { clearProperties: 1 },
            plugins:[block_Layout,block_ContentCards,block_DesignElement,gjsPresetWebpage,GrapesjsTabs,Accordions],
            pluginsOpts: {
              'grapesjs-accordions': {}
            },
            storageManager: {
              type: 'local',  
              autoload: false,
              autosave: false
            },
 
          });
            var openTmBtn = this.editorBuilder.Panels.getButton('views', 'open-tm');
            openTmBtn && openTmBtn.set('active', 1);
            var openSm = this.editorBuilder.Panels.getButton('views', 'open-sm');
            openSm && openSm.set('active', 1);
            const commands =  this.editorBuilder.Commands;
            commands.add('tlb-show-hide', editor => {
            editor.getSelected().setStyle({"display": 'none'});
            });
            commands.add('tlb-save', editor => {
            alert('Do Save');
            });

            commands.add('tlb-setting', editor => {
            console.log( editor.getSelected().child);
            svModel( this.editorBuilder);
            });
            commands.add('tlb-add-row', editor => {
            var domComponents = editor.DomComponents;
            domComponents.addComponent('<style>.gjs-row{display:table;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;width:100%;}<div data-gjs-type="default" draggable="true" data-highlightable="1" class="gjs-row" ><div data-gjs-type="default" draggable="true" data-highlightable="1" class="gjs-cell"></div></div>');
            });

            commands.add('tlb-add-cell', editor => {
            var domComponents = editor.DomComponents;
            const selected = editor.getSelected();
            // console.log( editor.getSelected().child);
            if(selected.get('components').length>0)
            {
            domComponents.addComponent('<div data-gjs-type="default" draggable="true" data-highlightable="1" class="gjs-row" ><div data-gjs-type="default" draggable="true" data-highlightable="1" class="gjs-cell"></div></div><style>.gjs-cell{width:8%;display:table-cell;height:75px;}</style>');
            }
            else
            {
            editor.runCommand('tlb-clone');
            }

            });
            this.editorBuilder.runCommand('sw-visibility');
            this.editorBuilder.Panels.addButton('commands', [ { id: 'save', className: 'btnSv', command: function(editor1, sender) {  editor1.store(); editor1.runCommand('open-blocks');  
            var dvTags=document.getElementsByClassName("gjs-clm-tags");
            var dvSectors=document.getElementsByClassName("gjs-sm-sectors");
            var dvHeader=document.getElementsByClassName("gjs-sm-header");
            if(dvTags)
            {
              document.getElementsByClassName("gjs-clm-tags")[0].parentElement.style.display="none";
            }
            if(dvSectors)
            {
              document.getElementsByClassName("gjs-sm-sectors")[0].parentElement.style.display="none";
            }
            if(dvHeader)
            {
             document.getElementsByClassName("gjs-sm-header")[0].parentElement.style.display="block";
            }
            }, attributes: { title: 'Save Template' } }, ]);
            this.editorBuilder.on('canvas:drop', (dataTransfer, model) => {
             
            if(model && model.ccid)
            {
              console.log(model.ccid);
              this.selComp=model.ccid;
            }
            if (model.is('banner')) {this.setState({isOpenBanner: true})}
            else if (model.is('2object')) {this.setState({isOpenO2: true})}
            else if (model.is('3object')) {this.setState({isOpenO3: true})}
            else if (model.is('4object')) {this.setState({isOpenO4: true})}
            else if (model.is('5object')) {this.setState({isOpenO5: true})}
            else if (model.is('6object')) {this.setState({isOpenObMore: true})}
            else if (model.is('uxaccordion')) {this.setState({isOpenUxAccordion: true})}
            else if (model.is('empttabs')) {this.setState({isOpenEmptyTab: true})}
            else if (model.is('map')) {this.setState({isOpenMap: true})}
            else if (model.is('htmlcode')) {this.setState({isOpenHTMLCode: true})}
            else if (model.is('heading')) {this.setState({isOpenHeadingCard: true})}
            else if (model.is('product')) {this.setState({isOpenProduct: true})}
            else if (model.is('category')) {this.setState({isOpenCategory: true})}
            else if (model.is('timer')) {this.setState({isOpenTimer: true})}
            else if (model.is('divider')) {this.setState({isOpenDivider: true})}
            else if (model.is('navigation')) {this.setState({isOpenNavigation: true})}
            else if (model.is('video')) {this.setState({isOpenVideo: true})}
            else if (model.is('newsbar')) {this.setState({isOpenNewsBar: true})}
            else if (model.is('tabCard')) {this.setState({isOpenTab: true})}
            else if (model.is('text')) {this.setState({isOpenText: true})}
            });
            const TOOLBAR_CELL = [
              {attributes: { class: "fa fa-arrow-up" },command: "tlb-move"},
              {attributes: { class: "fa fa-arrows" },command: "tlb-move"},
              {attributes: { class: "fa fa-cog" },command: "tlb-setting"},
              {attributes: {class: 'fa fa-clone'},command: 'tlb-clone'},
              {attributes: { class: "fa fa-eye" },command: 'tlb-show-hide'},
              {attributes: {class: 'fa fa-save'},command: 'tlb-save'},
              {attributes: {class: 'fa fa-trash-o'},command: 'tlb-delete'}
            ];
            const getCellToolbar = () => TOOLBAR_CELL;

            this.editorBuilder.on('component:selected', m => {
            m.set('toolbar', getCellToolbar()); // set a toolbars
            if(document.getElementsByClassName('gjs-traits-label') && document.getElementsByClassName('gjs-traits-label')[0] && document.getElementsByClassName('gjs-traits-label')[0].parentElement)
            {
            //document.getElementsByClassName('gjs-traits-label')[0].parentElement.classList.add("mystyle");
            document.getElementsByClassName('gjs-traits-label')[0].parentElement.style.display='block !important';
            }
             // }
            });
            const canvas =  this.editorBuilder.Canvas;
            const body= canvas.getBody();
            var styleEle = document.createElement('style');
            styleEle.innerHTML=".gjs-row{border:2px dotted blue !important;margin:5px; width:99%;}.gjs-cell{border:2px dotted orange !important;margin:5px;}";
            body.append(styleEle); 

            }
            toggleSettings()
            {
              
            }
            addingContent(d)
            {

            if(this.editorBuilder.DomComponents.getWrapper().find('#'+this.selComp) && this.editorBuilder.DomComponents.getWrapper().find('#'+this.selComp)[0])
            {
              const component =  this.editorBuilder.DomComponents.getWrapper().find('#'+this.selComp)[0];
              component.components(d);
            }     
      }
      render() {
      return (
        <>
        <div id="example-editor"></div>
        <Dialog isOpen={this.state.isOpenO2} onClose={(e)=>this.setState({isOpenO2:false})} cardName="2 Object">{<Object2 onClose={(e)=>this.setState({isOpenO2:false})} designData={this.addingContent.bind(this)}></Object2>}</Dialog>
        <Dialog isOpen={this.state.isOpenO3} onClose={(e)=>this.setState({isOpenO3:false})} cardName="3 Object">{<Object3 onClose={(e)=>this.setState({isOpenO3:false})} designData={this.addingContent.bind(this)}></Object3>}</Dialog>
        <Dialog isOpen={this.state.isOpenO4} onClose={(e)=>this.setState({isOpenO4:false})} cardName="4 Object">{<Object4 onClose={(e)=>this.setState({isOpenO4:false})} designData={this.addingContent.bind(this)}></Object4>}</Dialog>
        <Dialog isOpen={this.state.isOpenO5} onClose={(e)=>this.setState({isOpenO5:false})} cardName="5 Object">{<Object5 onClose={(e)=>this.setState({isOpenO5:false})} designData={this.addingContent.bind(this)}></Object5>}</Dialog>
        <Dialog isOpen={this.state.isOpenObMore} onClose={(e)=>this.setState({isOpenObMore:false})} cardName="More Object">{<MoreObject onClose={(e)=>this.setState({isOpenObMore:false})} designData={this.addingContent.bind(this)}></MoreObject>}</Dialog>
        <Dialog isOpen={this.state.isOpenBanner} onClose={(e)=>this.setState({isOpenBanner:false})} cardName="Banner Card">{<Banner onClose={(e)=>this.setState({isOpenBanner:false})} designData={this.addingContent.bind(this)}></Banner>}</Dialog>
        <Dialog isOpen={this.state.isOpenEmptyTab} onClose={(e)=>this.setState({isOpenEmptyTab:false})} cardName="Tab Template">{<EmptyTab onClose={(e)=>this.setState({isOpenEmptyTab:false})} designData={this.addingContent.bind(this)}></EmptyTab>}</Dialog>
        <Dialog isOpen={this.state.isOpenUxAccordion} onClose={(e)=>this.setState({isOpenUxAccordion:false})} cardName="Accordion Template">{<UxAccordion onClose={(e)=>this.setState({isOpenUxAccordion:false})} designData={this.addingContent.bind(this)}></UxAccordion>}</Dialog>
        <Dialog isOpen={this.state.isOpenMap} onClose={(e)=>this.setState({isOpenMap:false})} cardName="Map Card">{<Map onClose={(e)=>this.setState({isOpenMap:false})} designData={this.addingContent.bind(this)}></Map>}</Dialog>
        <Dialog isOpen={this.state.isOpenHTMLCode} onClose={(e)=>this.setState({isOpenHTMLCode:false})} cardName="HTML Code">{<HTMLCode onClose={(e)=>this.setState({isOpenHTMLCode:false})} designData={this.addingContent.bind(this)}></HTMLCode>}</Dialog>
        <Dialog isOpen={this.state.isOpenHeadingCard} onClose={(e)=>this.setState({isOpenHeadingCard:false})} cardName="Heading Card">{<HeadingCard onClose={(e)=>this.setState({isOpenHeadingCard:false})} designData={this.addingContent.bind(this)}></HeadingCard>}</Dialog>
        <Dialog isOpen={this.state.isOpenProduct} onClose={(e)=>this.setState({isOpenProduct:false})} cardName="Product">{<Product onClose={(e)=>this.setState({isOpenProduct:false})} designData={this.addingContent.bind(this)}></Product>}</Dialog>
        <Dialog isOpen={this.state.isOpenCategory} onClose={(e)=>this.setState({isOpenCategory:false})} cardName="Category">{<Category onClose={(e)=>this.setState({isOpenCategory:false})} designData={this.addingContent.bind(this)}></Category>}</Dialog>
        <Dialog isOpen={this.state.isOpenTimer} onClose={(e)=>this.setState({isOpenTimer:false})} cardName="Timer">{<Timer onClose={(e)=>this.setState({isOpenTimer:false})} designData={this.addingContent.bind(this)}></Timer>}</Dialog>
        <Dialog isOpen={this.state.isOpenDivider} onClose={(e)=>this.setState({isOpenDivider:false})} cardName="Divider">{<Divider onClose={(e)=>this.setState({isOpenDivider:false})} designData={this.addingContent.bind(this)}></Divider>}</Dialog>
        <Dialog isOpen={this.state.isOpenNavigation} onClose={(e)=>this.setState({isOpenNavigation:false})} cardName="Navigation">{<Navigation onClose={(e)=>this.setState({isOpenNavigation:false})} designData={this.addingContent.bind(this)}></Navigation>}</Dialog>  
        <Dialog isOpen={this.state.isOpenVideo} onClose={(e)=>this.setState({isOpenVideo:false})} cardName="Video">{<Video onClose={(e)=>this.setState({isOpenVideo:false})} designData={this.addingContent.bind(this)}></Video>}</Dialog>
        <Dialog isOpen={this.state.isOpenNewsBar} onClose={(e)=>this.setState({isOpenNewsBar:false})} cardName="News Bar">{<NewsBar onClose={(e)=>this.setState({isOpenNewsBar:false})} designData={this.addingContent.bind(this)}></NewsBar>}</Dialog> 
        <Dialog isOpen={this.state.isOpenTab} onClose={(e)=>this.setState({isOpenTab:false})} cardName="Tab">{<Tab onClose={(e)=>this.setState({isOpenTab:false})} designData={this.addingContent.bind(this)}></Tab>}</Dialog>
        <Dialog isOpen={this.state.isOpenText} onClose={(e)=>this.setState({isOpenText:false})} cardName="Text">{<Text onClose={(e)=>this.setState({isOpenText:false})} designData={this.addingContent.bind(this)}></Text>}</Dialog>
         </>
        );
      }
   }
   export default PageBuilder;