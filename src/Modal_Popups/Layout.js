import React, { Component } from "react";
import { ChromePicker } from "react-color";
import Select from "react-select";
import makeAnimated from "react-select/animated";
import modelcss from "./css/Modal_Popus.css";
import colorpickercss from "./css/color-picker.css";
import css from "./css/custom.css";s
const animatedComponents = makeAnimated();
const options = [
  { value: "101", label: "Segment1" },
  { value: "102", label: "Segment2" },
  { value: "103", label: "Segment3" },
];

const MyComponent = () => (
  <Select
    /*styles={customStyles}*/ options={options}
    closeMenuOnSelect={true}
    components={animatedComponents}
    /*defaultValue={[options[1], options[2]]}*/ isMulti
  />
);
class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      leftWidth: 29,
      rightWidth: 69,

      displayColorPicker: false,
      defaultColor: "#999",
      changeColor: "#999",
      //color:{r:'0',g:'9',b:'153',a:'1'},
      color: "#999",

      content_displayColorPicker: false,
      content_defaultColor: "#F39C12",
      content_changeColor: "#F39C12",
      content_color: { r: "0", g: "9", b: "153", a: "1" },

      main_displayColorPicker: false,
      main_defaultColor: "#8E44AD",
      main_changeColor: "#8E44AD",
      main_color: { r: "0", g: "9", b: "153", a: "1" },

      sub_displayColorPicker: false,
      sub_defaultColor: "#3498DB",
      sub_changeColor: "#3498DB",
      sub_color: { r: "0", g: "9", b: "153", a: "1" },

      selectedFile: null,
      imagePreviewUrl: null,
      isNormalLocation: true,
      isPersonalizedLocation: false,
      isSetting: true,
      istoggleFirstHeading: true,
      isLayout: true
    };
  }
  // image uploader START
  fileChangedHandler = (event) => {
    this.setState({
      selectedFile: event.target.files[0],
    });
    let reader = new FileReader();
    reader.onloadend = () => {
      this.setState({
        imagePreviewUrl: reader.result,
      });
    };
    reader.readAsDataURL(event.target.files[0]);
  };

  submit = () => {
    var fd = new FormData();
    fd.append("file", this.state.selectedFile);
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
      if (this.readyState === 4 && this.status === 200) {
        alert("Uploaded!");
      }
    };
    request.open(
      "POST",
      "https://us-central1-tutorial-e6ea7.cloudfunctions.net/fileUpload",
      true
    );
    request.send(fd);
  };
  // image uploader END

  onValueChange = (event) => {
    this.setState({
      leftWidth: event.target.value,
      rightWidth: 98 - event.target.value,
    });
  };
  onValueChangeFrame = (event) => {
    this.setState({
      // leftWidth:(event.target.value),rightWidth:(98-event.target.value)
      //selectedOption: event.target.value
    });
    // alert(event.target.value);
  };
  drawDesgin() {
    this.props.designData(
      '<div data-gjs-type="default" draggable="true"  class="gjs-row" ><div data-gjs-type="default" draggable="true" class="gjs-cell"></div></div>'
    );
    this.props.onClose(false);
  }
  closeThis() {
    this.props.onClose(false);
  }
  onHandleShowColorPicker = (e) => {
    var boxId = e.target.id;
    if (this.state.content_defaultColor) {
      this.setState({ content_displayColorPicker: false });
    }
    if (this.state.sub_displayColorPicker) {
      this.setState({ sub_displayColorPicker: false });
    }
    if (this.state.main_displayColorPicker) {
      this.setState({ main_displayColorPicker: false });
    }
    if (this.state.displayColorPicker) {
      this.setState({ displayColorPicker: false });
    }
    if (boxId === "clr-pkr-content-color") {
      this.setState({ content_displayColorPicker: true });
    } else if (boxId === "clr-pkr-color") {
      this.setState({ displayColorPicker: true });
    } else if (boxId === "clr-pkr-main-color") {
      this.setState({ main_displayColorPicker: true });
    } else if (boxId === "clr-pkr-sub-color") {
      this.setState({ sub_displayColorPicker: true });
    }
  };
  onChangeColorPicker = (color, event) => {
    this.setState({ color: color.rgb, changeColor: color.hex });
  };
  onChangeMainColorPicker = (color, event) => {
    this.setState({ main_color: color.rgb, main_changeColor: color.hex });
  };
  onChangeContentColorPicker = (color, event) => {
    this.setState({ content_color: color.rgb, content_changeColor: color.hex });
  };
  onChangeSubColorPicker = (color, event) => {
    this.setState({ sub_color: color.rgb, sub_changeColor: color.hex });
  };

  handleClose = (e) => {
    if (this.state.content_displayColorPicker) {
      this.setState({ content_displayColorPicker: false });
    }
    if (this.state.sub_displayColorPicker) {
      this.setState({ sub_displayColorPicker: false });
    }
    if (this.state.main_displayColorPicker) {
      this.setState({ main_displayColorPicker: false });
    }
    if (this.state.displayColorPicker) {
      this.setState({ displayColorPicker: false });
    }
  };
  showNormalLocation = (e) => {
    this.setState(
      {
        isNormalLocation: true,
        isPersonalizedLocation: false,
      },
      () => {
        console.log(this.state);
      }
    );
  };
  showPersonalizedLocation = (e) => {
    this.setState(
      {
        isNormalLocation: false,
        isPersonalizedLocation: true,
      },
      () => {
        console.log(this.state);
      }
    );
  };
  toggleFirstHeading = (e) => {
    this.setState({ istoggleFirstHeading: false }, () => {
    });
  };
  uptoggleFirstHeading = (e) => {
    this.setState({ istoggleFirstHeading: true }, () => {
    });
  };
  showSetting = (e) => {
    this.setState({ isSetting: false }, () => {
      console.log(this.state)
    });
  };
  upSetting = (e) => {
    this.setState({ isSetting: true }, () => {
      console.log(this.state)
    });
  };
  downLayout = (e) => {
    this.setState({ isLayout: false }, () => {
    });
  };
  upLayout = (e) => {
    this.setState({ isLayout: true }, () => {
    });
  };
  